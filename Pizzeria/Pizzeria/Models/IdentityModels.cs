﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Pizzeria.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Display(Name = "Imię")]
        public string Imie { get; set; }

        public string Nazwisko { get; set; }

        public string Ulica { get; set; }

        [Display(Name = "Numer domu")]
        public string NumerDomu { get; set; }

        [Display(Name = "Kod pocztowy")]
        public string KodPocztowy { get; set; }

        public string Miasto { get; set; }

        public string Adres
        {
            get
            {
                string dspUlica = string.IsNullOrWhiteSpace(this.Ulica) ? "" : this.Ulica;
                string dspNumerDomu = string.IsNullOrWhiteSpace(this.NumerDomu) ? "" : this.NumerDomu;
                string dspMiasto = string.IsNullOrWhiteSpace(this.Miasto) ? "" : this.Miasto;
                string dspKodPocztowy = string.IsNullOrWhiteSpace(this.KodPocztowy) ? "" : this.KodPocztowy;

                return string.Format("{0} {1}, {2} {3}", dspUlica, dspNumerDomu, dspKodPocztowy, dspMiasto);
            }
        }

        public string GoogleAdres
        {
            get
            {
                string dspUlica = string.IsNullOrWhiteSpace(this.Ulica) ? "" : this.Ulica;
                string dspNumerDomu = string.IsNullOrWhiteSpace(this.NumerDomu) ? "" : this.NumerDomu;
                string dspMiasto = string.IsNullOrWhiteSpace(this.Miasto) ? "" : this.Miasto;
                string dspKodPocztowy = string.IsNullOrWhiteSpace(this.KodPocztowy) ? "" : this.KodPocztowy;

                return string.Format("{0}{1},{2}{3}", dspUlica, dspNumerDomu, dspKodPocztowy, dspMiasto);
            }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}