﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class Edge
    {
        public int Id { get; set; }

        public List<Order> Orders { get; set; }

        [Display(Name = "Dystans [m]")]
        public int Distance { get; set; }

        [Display(Name = "Czas [s]")]
        public int Time { get; set; }

        public Edge()
        {
            Orders = new List<Order>();
        }

    }
}