﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class Order
    {
        [Display(Name = "Numer zamówienia")]
        public int Id { get; set; }

        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<Pizza> Pizzas { get; set; }

        [Display(Name = "Kwota zamówienia")]
        public int Value { get; set; }

        public DateTime Data { get; set; }

        public Order()
        {
            Value = 0;
            Data = DateTime.Now;
            Zrealizowane = false;
        }

        public int GetValue()
        {
            foreach (var pizza in Pizzas)
            {
                Value += pizza.Cena * pizza.Ilość;
            }

            return Value;
        }

        public bool Zrealizowane { get; set; }
    }
}