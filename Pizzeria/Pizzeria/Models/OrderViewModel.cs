﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class OrderViewModel
    {
        public List<Order> Orders = new List<Order>();

        public List<Edge> Edges = new List<Edge>();
    }
}