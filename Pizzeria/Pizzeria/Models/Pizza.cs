﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Pizzeria.Models
{
    public class Pizza
    {
        public int Id { get; set; }

        public string Nazwa { get; set; }

        public int Cena { get; set; }

        public string Składniki { get; set; }

        public string Photo { get; set; }

        [NotMapped]
        [Range(0, 5, ErrorMessage = "Przykro nam, maksymalnie można zamówić 5 pizz")]
        public int Ilość { get; set; }
    }
}