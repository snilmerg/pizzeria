﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Pizzeria.Models;

namespace Pizzeria.Controllers
{
    [Authorize(Users = "mpiorek95@gmail.com")]
    public class PizzasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Pizzas
        public ActionResult Index()
        {
            return View(db.Pizzas.ToList());
        }

        // GET: Pizzas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pizza pizza = db.Pizzas.Find(id);
            if (pizza == null)
            {
                return HttpNotFound();
            }
            return View(pizza);
        }

        // GET: Pizzas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pizzas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nazwa,Cena,Składniki")] Pizza pizza)
        {
            if (ModelState.IsValid)
            {
                db.Pizzas.Add(pizza);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pizza);
        }

        // GET: Pizzas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pizza pizza = db.Pizzas.Find(id);
            if (pizza == null)
            {
                return HttpNotFound();
            }
            return View(pizza);
        }

        // POST: Pizzas/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nazwa,Cena,Składniki")] Pizza pizza)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pizza).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pizza);
        }

        // GET: Pizzas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pizza pizza = db.Pizzas.Find(id);
            if (pizza == null)
            {
                return HttpNotFound();
            }
            return View(pizza);
        }

        // POST: Pizzas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pizza pizza = db.Pizzas.Find(id);
            db.Pizzas.Remove(pizza);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: Pizzas/Menu
        [HttpGet]
        public ActionResult Menu()
        {
            return View(db.Pizzas.ToList());
        }

        // POST: Pizzas/Menu
        [HttpPost]
        public ActionResult Menu(FormCollection values)
        {
            if (ModelState.IsValid)
            {
                var iloscistring = values.GetValues("Ilość");
                var ilosci = new List<int>();
                foreach(var x in iloscistring)
                {
                    ilosci.Add(int.Parse(x));
                }

                var pizze = db.Pizzas.ToList();

                for (int i = 0; i < pizze.Count(); i++)
                {

                    pizze[i].Ilość = ilosci[i];
                }

                pizze = pizze.Where(x => x.Ilość > 0).ToList();

                var aUserId = User.Identity.GetUserId();
                var aUser = db.Users.FirstOrDefault(x => x.Id == aUserId);

                var zamowienie = new Order()
                {
                    Pizzas = pizze as ICollection<Pizza>,
                    UserId = aUserId,
                    User = aUser
                };

                zamowienie.GetValue();

                db.Orders.Add(zamowienie);
                db.SaveChanges();
            }

            return View(db.Pizzas.ToList());
        }
    }
}
