﻿namespace Pizzeria.Migrations
{
    using Microsoft.AspNet.Identity;
    using Pizzeria.Models;
    using Pizzeria.Controllers;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Pizzeria.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Pizzeria.Models.ApplicationDbContext context)
        {
            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 1,
                Cena = 19,
                Nazwa = "Hawajska",
                Składniki = "sos, mozarella, ananas, szynka, pieczarki",
                Photo = "/Resources/margherita.jpg"
            });

            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 2,
                Cena = 16,
                Nazwa = "Margherita",
                Składniki = "sos, mozarella",
                Photo = "/Resources/margherita.jpg"
            });

            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 3,
                Cena = 23,
                Nazwa = "Parma",
                Składniki = "sos, mozarella, szynka parmeńska, rucola, pieczarki",
                Photo = "/Resources/margherita.jpg"
            });

            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 4,
                Cena = 20,
                Nazwa = "Quattro Formaggi",
                Składniki = "sos, sery: mozarella, pleśniowy blue, cheddar, parmezan",
                Photo = "/Resources/margherita.jpg"
            });

            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 5,
                Cena = 22,
                Nazwa = "Hell's Kitchen",
                Składniki = "sos, mozarella, jalapeno, bekon, cebula, pepperoni",
                Photo = "/Resources/margherita.jpg"
            });

            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 6,
                Cena = 19,
                Nazwa = "Wegetariańska",
                Składniki = "sos, mozarella, papryka, pomidor, cebula, ogórek",
                Photo = "/Resources/margherita.jpg"
            });

            context.Pizzas.AddOrUpdate(new Pizza
            {
                Id = 7,
                Cena = 23,
                Nazwa = "Alternative",
                Składniki = "sos, mozarella, kurczak, bekon, cebula, sos barbecue",
                Photo = "/Resources/margherita.jpg"
            });

            context.SaveChanges();

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
