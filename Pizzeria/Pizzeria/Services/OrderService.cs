﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using Pizzeria.Models;
using Pizzeria.Services;

namespace Pizzeria.Services
{
    public class OrderService
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public OrderViewModel owm = new OrderViewModel();         
    
        private string pizzeria = "PolitechnikaWrocławska";

        protected string fileGetContents(string fileName)
        {
            string sContents = string.Empty;
            string me = string.Empty;
            try
            {
                if (fileName.ToLower().IndexOf("https:") > -1)
                {
                    System.Net.WebClient wc = new System.Net.WebClient();
                    byte[] response = wc.DownloadData(fileName);
                    sContents = System.Text.Encoding.ASCII.GetString(response);

                }
                else
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(fileName);
                    sContents = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch { sContents = "unable to connect to server "; }
            return sContents;
        }

        public List<int> getDistanceAndTime(string origin, string destination)
        {
            System.Threading.Thread.Sleep(1000);
            int distance = 0;
            int time = 0;
            string requesturl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&sensor=false&key=AIzaSyBftGsAN1bdSpHAKyJHDtN8tNfMZuimZbQ";

            string content = fileGetContents(requesturl);

            JObject o = JObject.Parse(content);

            distance = (int)o.SelectToken("routes[0].legs[0].distance.value");
            time = (int)o.SelectToken("routes[0].legs[0].duration.value");

            return new List<int>{ distance, time };
        }

        public Edge getDistanceAndTime4Orders(Order origin, Order destination)
        {
            System.Threading.Thread.Sleep(1000);
            int distance = 0;
            int time = 0;
            string requesturl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin.User.GoogleAdres + "&destination=" + destination.User.GoogleAdres + "&sensor=false&key=AIzaSyBftGsAN1bdSpHAKyJHDtN8tNfMZuimZbQ";

            string content = fileGetContents(requesturl);

            JObject o = JObject.Parse(content);

            distance = (int)o.SelectToken("routes[0].legs[0].distance.value");
            time = (int)o.SelectToken("routes[0].legs[0].duration.value");

            return new Edge { Distance = distance, Time = time, Orders = new List<Order> { origin, destination} };
        }

        public Edge getDistanceAndTime4Pwr(Order destination)
        {
            System.Threading.Thread.Sleep(1000);
            int distance = 0;
            int time = 0;
            string requesturl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + pizzeria + "&destination=" + destination.User.GoogleAdres + "&sensor=false&key=AIzaSyBftGsAN1bdSpHAKyJHDtN8tNfMZuimZbQ";

            string content = fileGetContents(requesturl);

            JObject o = JObject.Parse(content);

            distance = (int)o.SelectToken("routes[0].legs[0].distance.value");
            time = (int)o.SelectToken("routes[0].legs[0].duration.value");

            return new Edge { Distance = distance, Time = time, Orders = new List<Order>() { destination } };
        }

        public int getDistance(string origin, string destination)
        {
            System.Threading.Thread.Sleep(1000);
            string requesturl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&sensor=false&key=AIzaSyBftGsAN1bdSpHAKyJHDtN8tNfMZuimZbQ";

            string content = fileGetContents(requesturl);

            JObject o = JObject.Parse(content);

            return (int)o.SelectToken("routes[0].legs[0].distance.value");
        }

        public int getTime(string origin, string destination)
        {
            System.Threading.Thread.Sleep(1000);
            string requesturl = "https://maps.googleapis.com/maps/api/directions/json?origin=" + origin + "&destination=" + destination + "&sensor=false&key=AIzaSyBftGsAN1bdSpHAKyJHDtN8tNfMZuimZbQ";

            string content = fileGetContents(requesturl);

            JObject o = JObject.Parse(content);

            return (int)o.SelectToken("routes[0].legs[0].duration.value");
        }

        public void OrganizeOrdersPwr(List<Order> zamówienia)
        {
            var pwrDistances = new List<Edge>();

            foreach (var el in zamówienia)
            {
                var edge = getDistanceAndTime4Pwr(el);
                pwrDistances.Add(edge);
            }

            var najkrotszyCzas = pwrDistances.Min(x => x.Time);
            var edg = pwrDistances.First(x => x.Time == najkrotszyCzas);

            var wybrany = edg.Orders.First();

            owm.Orders.Add(wybrany);
            owm.Edges.Add(edg);

            zamówienia.Remove(wybrany);

            if (zamówienia.Count() == 0)
            {
                return ;
            }
            else
            {
                OrganizeOrders(zamówienia, wybrany);
            }
        }

        public void OrganizeOrders(List<Order> zamówienia, Order current)
        {
            var Distances = new List<Edge>();

            foreach (var next in zamówienia)
            {
                 Distances.Add(getDistanceAndTime4Orders(current, next));
            }

            var najkrotszyCzas = Distances.Min(x => x.Time);
            var edg = Distances.First(x => x.Time == najkrotszyCzas);

            var wybrany = edg.Orders.Last();

            owm.Orders.Add(wybrany);
            owm.Edges.Add(edg);

            zamówienia.Remove(wybrany);

            if (zamówienia.Count() == 0)
            {
                return;
            }
            else
            {
                OrganizeOrders(zamówienia, wybrany);
            }
        }
    }
}